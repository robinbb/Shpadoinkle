name:         Shpadoinkle-examples
license:      BSD3
license-file: LICENSE
version:      0.0.0.3
author:       Isaac Shapira
maintainer:   fresheyeball@protonmail.com
category:     Web
build-type:   Simple
synopsis:     Example usages of Shpadoinkle
description:  A collection of illustrative applications to show various Shpadoinkle utilities.


ghc-options:
    - -Wall
    - -Wcompat
    - -fwarn-redundant-constraints
    - -fwarn-incomplete-uni-patterns
    - -fwarn-tabs
    - -fwarn-incomplete-record-updates
    - -fwarn-identities


ghcjs-options:
    - -Wall
    - -Wcompat
    - -fno-warn-missing-home-modules
    - -fwarn-redundant-constraints
    - -fwarn-incomplete-uni-patterns
    - -fwarn-tabs
    - -fwarn-incomplete-record-updates
    - -fwarn-identities
    - -dedupe
    - -O2


extra-source-files:
  - README.md
  - CHANGELOG.md


dependencies:
    - base >= 4.12.0 && < 4.16


executables:
  todomvc:
    main: TODOMVC.hs
    other-modules: []
    source-dirs: .
    dependencies:
      - text
      - containers
      - generic-lens
      - lens
      - pretty-show

      - Shpadoinkle
      - Shpadoinkle-html
      - Shpadoinkle-lens
      - Shpadoinkle-backend-snabbdom

  lens:
    main: Lens.hs
    other-modules: []
    source-dirs: .
    dependencies:
      - text
      - generic-lens
      - lens
      - safe

      - Shpadoinkle
      - Shpadoinkle-html
      - Shpadoinkle-lens
      - Shpadoinkle-backend-pardiff

  counter:
    main: Counter.hs
    other-modules: []
    source-dirs: .
    dependencies:
      - text
      - stm

      - Shpadoinkle
      - Shpadoinkle-html
      - Shpadoinkle-backend-pardiff

  calculator:
    main: Calculator.hs
    other-modules: []
    source-dirs: .
    dependencies:
      - text
      - safe
      - lens

      - Shpadoinkle
      - Shpadoinkle-html
      - Shpadoinkle-lens
      - Shpadoinkle-backend-pardiff
      - Shpadoinkle-widgets

  calculator-ie:
    main: CalculatorIE.hs
    other-modules: []
    source-dirs: .
    dependencies:
      - aeson
      - text
      - safe
      - lens
      - split
      - file-embed

      - Shpadoinkle
      - Shpadoinkle-console
      - Shpadoinkle-html
      - Shpadoinkle-backend-pardiff
      - Shpadoinkle-widgets

  animation:
    main: Animation.hs
    other-modules: []
    source-dirs: .
    dependencies:
      - Shpadoinkle
      - Shpadoinkle-backend-snabbdom
      - Shpadoinkle-html
      - text
      - ease
      - ghcjs-dom
      - stm
      - unliftio

  widgets:
    main: Widgets.hs
    source-dirs: ./widgets
    dependencies:
      - lens
      - text
      - stm

      - Shpadoinkle
      - Shpadoinkle-backend-pardiff
      - Shpadoinkle-backend-snabbdom
      - Shpadoinkle-html
      - Shpadoinkle-lens
      - Shpadoinkle-widgets

  throttle-and-debounce:
    main: ThrottleAndDebounce.hs
    other-modules: []
    source-dirs: .
    dependencies:
     - text
     - safe
     - lens

     - Shpadoinkle
     - Shpadoinkle-console
     - Shpadoinkle-html
     - Shpadoinkle-backend-pardiff

  servant-crud-client:
    main: Run/Client.hs
    source-dirs: ./servant-crud
    other-modules:
      - Types
      - Types.Prim
      - View
      - Client
    dependencies:
      - aeson
      - bytestring
      - beam-core
      - containers
      - jsaddle
      - lens
      - text
      - stm
      - servant
      - mtl
      - unliftio
      - exceptions

      - Shpadoinkle
      - Shpadoinkle-backend-pardiff
      - Shpadoinkle-backend-snabbdom
      - Shpadoinkle-html
      - Shpadoinkle-widgets
      - Shpadoinkle-router
      - Shpadoinkle-lens
    when:
      - condition: impl(ghcjs)
        then:
          dependencies:
            - servant-client-js
        else:
          dependencies:
            - servant-client
            - beam-sqlite
            - sqlite-simple

  servant-crud-server:
    when:
      - condition: impl(ghcjs)
        then:
          buildable: False
        else:
          buildable: True
    main: Run/Server.hs
    source-dirs: ./servant-crud
    other-modules:
      - Types.Prim
      - Types
      - View
      - Server
    dependencies:
      - aeson
      - bytestring
      - beam-core
      - beam-sqlite
      - containers
      - file-embed
      - text
      - lens
      - stm
      - mtl
      - sqlite-simple
      - servant
      - servant-server
      - optparse-applicative
      - wai
      - wai-app-static
      - warp

      - Shpadoinkle
      - Shpadoinkle-html
      - Shpadoinkle-widgets
      - Shpadoinkle-router
      - Shpadoinkle-backend-static
      - Shpadoinkle-lens

  servant-crud-dev:
    when:
      - condition: impl(ghcjs)
        then:
          buildable: False
        else:
          buildable: True
    main: Run/Dev.hs
    source-dirs: ./servant-crud
    other-modules:
      - Types.Prim
      - Types
      - View
      - Client
      - Server
    dependencies:
      - aeson
      - bytestring
      - beam-core
      - beam-sqlite
      - containers
      - file-embed
      - text
      - lens
      - stm
      - mtl
      - exceptions
      - jsaddle
      - unliftio
      - sqlite-simple
      - servant
      - servant-server
      - optparse-applicative
      - wai
      - wai-app-static
      - warp
      - generic-monoid

      - Shpadoinkle
      - Shpadoinkle-html
      - Shpadoinkle-widgets
      - Shpadoinkle-router
      - Shpadoinkle-backend-static
      - Shpadoinkle-backend-snabbdom
      - Shpadoinkle-backend-pardiff
      - Shpadoinkle-lens



git: https://gitlab.com/fresheyeball/Shpadoinkle.git
